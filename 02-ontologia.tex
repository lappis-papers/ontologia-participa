\section{Descrição da ontologia de participação social}
\label{sec:descricao}

O modelo conceitual refere-se ao processo de participação social no Brasil e é fruto de discussões realizadas com os técnicos do IPEA (Instituto de Pesquisa Econômica Aplicada) e do extinto Departamento de Participação Social da Secretaria Nacional de Articulação Social - SNAS sobre \textit{(i)} os aspectos de organização da informação/conhecimento na perspectiva da \textit{web} semântica e, consequentemente, \textit{(ii)} da utilização de ontologias nesse processo. Embora não tenha havido reuniões sistemáticas para identificação de conceitos, o método utilizado para a construção dessa ontologia estendida foi baseado na experiência de~\cite{Brusa2006}.

Dentre os pressupostos principais para o modelo conceitual apresentado está a Teoria da Estruturação de \cite{Giddens1986,ODwyer2010}, que tem sido considerada por diversos autores para descrição dos processos sociais de participação social. Nessa perspectiva, os participantes da sociedade civil e do Governo são mobilizadores em igual escala de importância para realização do processo democrático. Essa é uma suposição interessante que pode ser apropriada em ambientes virtuais de participação social, uma vez que o nível de alcance dessas tecnologias é ilimitado e já tem feito diferença quando o assunto é mobilização de massas.

Após algumas entrevistas e consultas feitas aos materiais disponíveis sobre canais formais e informais de participação social, chegou-se a um conjunto de conceitos relacionados aos atores e canais de participação social, mais especificamente de conselhos e conferências. As fontes utilizadas para identificação desses conceitos foram conferidas nos \textit{sites} de conselhos e documentos de conferências nacionais, além do Vocabulário Controlado do Governo Eletrônico (VCGE)\footnote{\url{http://governo eletronico.gov.br/biblioteca/arquivos/vocabulario-controlado-do-governo-eletronico-vcge/download}}.

Além disso, foram investigadas outras referências para o tratamento de aspectos relacionados \textit{(i)} à gestão da participação, \textit{(ii)} ao processo democrático em si e, finalmente, \textit{(iii)} de outras ontologias já criadas sobre participação social em ambientes virtuais. Em função dos elementos encontrados, foi feito um desenho esquemático da OPS, o qual está apresentado na Figura \ref{figura01}. Nesse caso, a participação social é um conceito central que está intimamente ligado às classes \textit{AtorPS}, \textit{CanalPS}, \textit{ProcessoDemocrático}, \textit{Projeto} e \textit{Plataforma}, os quais estão descritas mais adiante.

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{figuras/figura01}
\caption{Ontologia básica de participação social.}
\label{figura01}
\end{figure}

Na Figura \ref{figura01}, as classes são identificadas por retângulos com fundo em cor mais clara e as relações (\textit{object properties} por retângulos na cor preta; o \textit{range} das relações é representado por uma letra ``r'', enquanto o \textit{domain} é representado pela letra ``d''. Os elementos da classe \textit{ParticipacaoSocial} são definidos por terem uma relação unívoca (1:1, i.e., um-para-um) com um elemento de cada uma das classes referentes às demais classes apresentadas na Figura \ref{figura01} e o detalhamento de cada uma delas está apresentado nas próximas sub-seções.

Na ontologia proposta, a principal classe chama-se \textit{ParticipacaoSocial} e refere-se à descrição genérica do que seja participação social. Nesse caso, qualquer participação democrática, seja ela formal ou informal, é considerada uma instância da classe principal e, por isso, pode ser descrita com base nos conceitos e relações expostos neste documento. Portanto, a participação social pode ser entendida como um conjunto de instâncias ou eventos de participação social que são caracterizados por terem um vínculo com um canal de participação e com atores de participação (representantes do Governo e da sociedade civil). Ao mesmo tempo, a participação social envolve um processo democrático, calculado por um projeto e, no caso dos ambientes virtuais de participação, contemplam uma plataforma de apoio ao processo como um todo. 

Nessa primeira versão da ontologia, os conceitos relacionados à gestão da participação e parte das especificações das classes \textit{ProcessoDemocratico}, \textit{Projeto} e \textit{Plataforma} foram adaptados das ideias propostas por~\cite{Porwol2014}. Essa solução foi adotada por serem compatíveis com as necessidades identificadas, embora essa seja uma estratégia que necessite ser revista em versões futuras. Os detalhes de cada uma dessas classes estão apresentados nas subseções a seguir.

\subsection{Atores de participação social}

A participação social é um processo que envolve atores com diferentes características. Em função da sua importância, os atores foram detalhados em conceitos e propriedades descritas na Figura \ref{figura02}.

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{figuras/figura02}
\caption{Classes sobre atores de participação social.}
\label{figura02}
\end{figure}

Nessa figura, o ator de participação social pode ser alguém ligado ao processo de gestão do evento de participação ou ser um participante comum, que pode ou não estar identificado formalmente no evento. No caso dos conselhos, foram explicitadas os papéis do presidente, do vice-presidente e do secretário executivo, enquanto que para as conferências foram ressaltadas o coordenador da conferência, o \textit{stakeholder} e o mediador, que articula e motiva os demais participantes a se engajarem no processo democrático.

De fato, um evento de participação social pode contemplar, em alguns casos, a presença de pessoas não identificadas e, mesmo assim, terem algum nível de participação. Por sua vez, os participantes identificados podem ser \textit{(i)} um conselheiro, que compõe formalmente um conselho (conselho nacional de saúde, conselho nacional de educação etc.), ou \textit{(ii)} um conferencista, aquele que participa de conferências temáticas (conferência de direitos humanos, conferência de política para as mulheres etc.). Nesse último caso, é importante ressaltar que as conferências possuem etapas que se iniciam no município e vão até a esfera federal. Assim, um conferencista pode assumir o papel de delegado, ou seja, aquele que foi eleito para representar o grupo na etapa seguinte.

Num processo conferencial é de praxe a existência de \textit{(i)} uma comissão organizadora nacional que colabora na gestão de todo o processo, e \textit{(ii)} de comissões organizadoras nos estados e municípios, que fazem a gestão local. Um representante de uma entidade que faz parte dessa comissão, ainda que não ocupe um cargo de gestor, realiza o papel de fiador do processo e é definido como um \textit{stakeholder}. Ou seja, no seu espaço de atuação, o \textit{stakeholder} é importante para o processo democrático e para a participação social como um todo.

Por sua vez, a figura de gestor contempla vários papéis, alguns dos quais estão explicitados na figura e devem ser aplicados, em função do tipo de canal de participação social. O perfil do participante envolve Governo e sociedade, sendo que o participante tem um perfil diverso e contempla um nível de engajamento.

O perfil relacionado às classes \textit{Diversidade} e \textit{Raça}, ilustradas na Figura \ref{figura02}, define um caráter mais democrático ou não para o processo participativo. Por sua vez, esses elementos foram apresentados considerando comunidades variadas, partindo-se da premissa de que a participação social é tão mais profícua quanto mais diverso é o público participante tanto no que se refere aos aspectos de raça e gênero, quanto com relação às entidades que esses participantes representam. Nesse caso, tanto a diversidade quanto a raça são consideradas em função daquilo que é declarado normalmente pelos participantes.

Na Figura \ref{figura02} existem ainda diversas relações que explicitam como os atores se ligam a outros conceitos da ontologia. Por exemplo, \textit{EntidadeRepresentada} é a relação entre os participantes e as entidades que elas representam (cidade, região, movimento social etc.). Da mesma forma, \textit{temEngajamento} define o nível de engajamento dos atores de participação social.

\subsection{Canais de participação social}

A participação social ocorre por canais apropriados e estes, por sua vez, podem ser formais ou informais. A formalidade está no fato de que o canal é reconhecido em instâncias de Governo e geralmente há uma norma legal que o define, ao contrário do que ocorre com o canal informal. Atores de participação social se vinculam a canais de participação social atuando de acordo com a característica desse meio de diálogo democrático. Em geral, os canais de participação são associadas a uma determinada área política e são o elo do Governo com a sociedade civil. Como exemplo de áreas políticas, pode-se citar as relacionadas a desenvolvimento econômico, política social, garantia de direitos, entre outras.

Essas áreas políticas são uma tentativa de categorizar as demandas sociais para facilitar a sua identificação e, por consequência, melhorar a qualidade do diálogo entre as partes. Percebe-se que os canais de participação são separados por área política e esses podem ter caráter presencial ou virtual (no caso do \textit{e-Participation}). Independente do tipo de canal, em geral existe um processo de gestão estabelecido e que cicla em períodos definidos. No caso da ontologia proposta, essa gestão está ligada a conselhos e conferências, embora existam diversos outros canais de participação que podem ser incluídos nessa abordagem.

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{figuras/figura03}
\caption{Classes sobre canais de participação social.}
\label{figura03}
\end{figure}

Canais formais, geralmente, são ligados a órgãos específicos de governo, tais como ministérios e secretarias da Administração Pública Federal. Na Figura \ref{figura03} estão representados os conceitos ligados aos canais de participação, considerando apenas as instâncias federais de participação social. A classe \textit{CanalPS} é a mais genérica que representa os canais de participação social, que podem ser formais ou informais. Por exemplo, a classe \textit{Conferência} é uma classe que descreve um canal formal temporário que é ligado a um órgão de Governo e, eventualmente a um determinado conselho nacional. Por outro lado, a classe \textit{GestaoConselho} refere-se aos diversos ciclos de gestão de um determinado conselho. Por sua vez, o conceito de \textit{AreaPolítica} refere-se a uma forma de caracterizar os canais de participação social para fins de acompanhamento das políticas em discussão. Áreas políticas referem-se, portanto, à subclasses ligadas ao desenvolvimento econômico, recursos naturais e outros conceitos que organizam canais, temas e órgãos de Governo.

\subsection{Processo democrático de participação social}

O processo democrático impulsiona toda a iniciativa de participação social e é o cerne de tudo o que se discute neste artigo. Em função de sua importância, a especificação feita aqui partiu das reflexões sobre a realidade local e, além disso, baseou-se na teoria da estruturação de \cite{Giddens1986}. Nessa abordagem, assume-se que o agente participante é limitado pela estrutura estabelecida, mas pode agir e alterá-la em função de sua autonomia. Ao mesmo tempo, o Governo, enquanto representante da estrutura pode ser um catalisador do processo de participação, realizando convocações e organizando a dinâmica no processo participativo.

Além da teoria da estrutura, as definições apresentadas foram retiradas das recomendações de ontologia feitas por~\cite{Porwol2014}, considerando as devidas adaptações para os processos participativos em discussão no Brasil. Nessa linha de raciocínio, cabe ao processo democrático a definição da missão maior envolvida. Nesse caso, a missão se refere ao tratamento e a formulação de políticas públicas, bem como, aos atores principais responsáveis por essas políticas, chamados aqui de \textit{stakeholders}. Além disso, o processo inclui conceitos sobre a execução em si e dos instrumentos democráticos disponíveis. No caso do \textit{e-Participation}, esses instrumentos são, em geral, \textit{software} que implementam um dinamismo e uma organicidade ao processo participativo, como é caso das trilhas de participação do portal Participa.br.

O escopo (\textit{domain}), os resultados esperados e o objetivo do processo democrático baseiam-se no fato de que esse precisa ter um gatilho que dispara um início e que evoluirá até que um fim seja alcançado. A definição formal desse processo é usada para gerar um projeto de participação relevante (discutido mais adiante). Nessa perspectiva, entram elementos relacionados a custo e recursos financeiros e essa atividade, geralmente, é objeto de trabalho do \textit{stakeholder} do processo democrático.

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{figuras/figura04}
\caption{Classes sobre o processo democrático.}
\label{figura04}
\end{figure}

A Figura \ref{figura04} apresenta as classes e as relações que caracterizam toda a dinâmica do processo democrático. Por exemplo, a classe \textit{InstrumentoDemocratico} descreve formas de fazer acontecer o processo participativo. Nesse caso, independente de ser presencial ou não, esse processo necessita de estratégias que devem ser propostas para os participantes a fim de que exerçam a participação profícua no debate. Dentre os diversos instrumentos disponíveis, estão os grupos de trabalho, oficinas e palestras para formação de senso crítico, além de \textit{softwares} especiais e outros voltados para incrementar o debate democrático. Por outro lado, a classe \textit{PoliticaPublica} descreve que todo processo democrático, dentro da perspectiva estabelecida de participação social envolve uma execução e, geralmente, uma descrição ou acompanhamento de política pública. Nesse caso, assume-se que o processo democrático pode ser algo instanciado pelo próprio Governo a fim de delimitar uma política pública e garantir a sua execução.

\subsection{Projeto de participação social}

A participação social exige um projeto formalizado ou não para garantir o seu sucesso, dentro das condições estabelecidas pelos fatores que influenciam o processo, tais como recursos materiais e financeiros. A análise da participação social é incompleta se esses elementos não forem considerados para garantir o efeito desejado~\cite{Porwol2014}. Nessa perspectiva, tanto membros da sociedade quanto do Governo podem iniciar um projeto de participação social e, em qualquer dos casos, deve-se ter em mente os objetivos, resultados desejados, tempo estimado para início e fim do processo e uma visão contábil e gerencial de toda a dinâmica de participação social.

A OPS contempla classes e propriedades de gestão que, de acordo com a Teoria das Capacidades Dinâmicas discutida em \cite{Wang2007}, justifica as vantagens competitivas que possuem inovação tecnológica, como é o caso dos ambientes virtuais de participação social. De fato, a abordagem teórica das Capacidades Dinâmicas faz parte do campo da Gestão Estratégica na Administração e procura explicar a maneira como as empresas atingem e mantêm vantagem competitiva frente às adversárias de modo sustentável, sobretudo em ambientes caracterizados pela inovação tecnológica\footnote{\url{http://pt.wikipedia.org/wiki/Capacidades_dinamicas}}. Como a OPS é voltada para ambientes virtuais ou plataformas de participação social, a estratégia de ~\cite{Porwol2014} foi absorvida sem modificações nessa ontologia proposta. Nesse caso, defende-se a suficiente divulgação do processo participativo dentro das restrições definidas a fim de maximizar o impacto do projeto. Aqui, o impacto esperado tem de ser definido como uma medida de avaliação e alinhado com o objetivo definido. Por sua vez, esse será, finalmente, expresso através de uma medida de desempenho. O projeto utiliza os recursos atribuídos para participação (virtual ou presencial) e esses são facilitados pela equipe de gerenciamento de projetos.

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{figuras/figura05}
\caption{Classes de projeto.}
\label{figura05}
\end{figure}

No caso da \textit{e-Participation}, é preciso prever gastos com a plataforma de participação com a contratação de agentes externos ou mesmo resolução de problemas com infraestrutura local ao ambiente de gestão da plataforma. Essa é, de fato, uma preocupação que deve estar constantemente no radar dos envolvidos com a gestão dos ambientes virtuais de participação social. Uma visão geral dos conceitos e relações apresentados, além de outros relacionados à etapa de projeto num evento de participação social estão apresentados na Figura \ref{figura05}, cujas classes podem ser identificadas nos módulos relacionados ao processo democrático e à plataforma de \textit{e-Participation}. O que muda em relação aos usos dessas classes nos demais módulos é a perspectiva de resultados esperados, de acordo com o viés de cada um deles.

A título de exemplo, o conceito \textit{Gerenciamento} refere-se aos aspectos procedurais assumidos para garantir que o processo participativo aconteça de acordo com o planejado, enquanto que a classe \textit{Disseminacao} representa a preocupação do \textit{stakeholder} em divulgar o processo participativo para alcançar bons resultados.

\subsection{Participação social em ambientes virtuais}

A plataforma de \textit{e-Participation}, deve ser concebida para contemplar ferramentas que permitam rapidez e facilidade para a comunicação dos cidadãos entre si e com os tomadores de decisão. Os envolvidos no diálogo são elencados em função do sentido estabelecido: do cidadão para o Governo ou do Governo para o cidadão. Em qualquer dos casos, é preciso ter um retorno (\textit{feedback}) para os envolvidos no debate virtual. Por sua vez, a \textit{e-Participation} acontece por meio de anotações e atribuição de \textit{tags} a documentos textuais e não textuais disponibilizados pelos \textit{stakeholders}. Além dessa técnica, existem outras que viabilizam a comunicação de forma mais ou menos estruturada, de acordo com a ferramenta disponível. Por exemplo, as discussões tendem a ser mais organizadas do que os fóruns já que no primeiro é possível estabelecer um \textit{ranking} de tópicos mais comentados. De qualquer modo, o tipo de comunicação fornecido para o participante pode ser síncrono como um \textit{live-chat} ou assíncrono como um fórum ou \textit{blog}.

Mesmo em ambientes virtuais, o processo democrático deve ser balizado por uma iniciativa ou domínio e por um escopo. Além disso, a discussão que ocorre na plataforma deve ser estendida para outros espaços externos ao ambiente virtual, possivelmente com deliberações sendo decididas a partir de divulgação em redes sociais. Da mesma forma, prevê-se, em função desse grau de abertura, que as redes sociais sejam monitoradas até mesmo para se ter uma ideia dos resultados provenientes de espaços que fogem ao controle dos administradores do ambiente virtual.

Assim como em ambientes não virtuais deseja-se manter um controle sobre o desempenho do processo participativo, o ambiente virtual deve prover mecanismos de avaliação e utilizá-los de acordo com medidas de desempenho técnico definidos na fase de projeto do processo participativo.

Em ambientes virtuais é comum ocorrerem sobrecargas de informações em função da capilaridade de uso e do alcance do ambiente virtual. Para facilitar a exploração das informações envolvidas nas discussões, sugere-se que sejam realizados resumos de forma automática ou manual para estarem disponíveis aos interessados. De fato, em debates bem divulgados na Internet, é comum serem recuperadas algumas centenas de milhares de comentários e opiniões dos participantes. Sem uma estratégia de síntese, esses dados ficam perdidos.

A plataforma virtual deve também prover resultados para os participantes, dado que está previsto também um \textit{ranking} de satisfação do usuário com relação ao \textit{e-Participation}. Portanto, o resultado de um debate em conjunto com o \textit{ranking} de satisfação do usuário e um valor de desempenho técnico são elementos importantes para serem acompanhados em plataformas virtuais. Além disso, é interessante prover relatórios com os principais resultados de projeto de participação social, formando uma memória de processos ocorridos, a fim de se criar indicadores para novas investidas no contexto da participação virtual.

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{figuras/figura06}
\caption{Classes do \textit{e-Participation}.}
\label{figura06}
\end{figure}

Na Figura \ref{figura06} constam classes e propriedades relacionadas a essas e outras características ontológicas de \textit{e-Participation}. Por exemplo, a classe \textit{Ferramenta} é a classe que representa as ferramentas de participação social disponíveis aos usuários, enquanto que a classe \textit{Monitoramento} refere-se ao processo de monitoramento das discussões que podem ser captados por ambientes virtuais sobre resultados provenientes de usuários que estão registrados ou não na plataforma. Essa classe está relacionada à preocupação em monitorar informações das redes sociais sobre temas no contexto das discussões de participação social.
